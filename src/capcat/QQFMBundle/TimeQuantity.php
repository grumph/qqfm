<?php

namespace capcat\QQFMBundle;

/**
 * Perhaps later consider using a package like :
 * https://packagist.org/packages/granam/date-interval
 * For now, php 5.6 is a requirement so I made this dirty class
 */




class TimeQuantity
{
    const DAYS_IN_MONTH = 30.4; // = 365 / 12

    private $seconds;

    public function __construct( int $seconds = null )
    {
        if ($seconds === null)
        {
            $seconds = 0;
        }
        $this->seconds = $seconds;
    }

    public function set( $seconds )
    {
        $this->seconds = $seconds;
    }

    public function get()
    {
        return $this->seconds;
    }

    public function setFromDetails($month, $week, $day, $hour, $minute, $second)
    {
        $this->seconds = (int)($second
                               + $minute * 60
                               + $hour   * 60 * 60
                               + $day    * 60 * 60 * 24
                               + $week   * 60 * 60 * 24 * 7
                               + $month  * 60 * 60 * 24 * self::DAYS_IN_MONTH);
    }

    public function getArray()
    {
        $seconds = $this->seconds;
        $month = (int)($seconds / (60 * 60 * 24 * self::DAYS_IN_MONTH));
        $seconds = $seconds % (60 * 60 * 24 * self::DAYS_IN_MONTH);
        $week = (int)($seconds / (60 * 60 * 24 * 7));
        $seconds = $seconds % (60 * 60 * 24 * 7);
        $day = (int)($seconds / (60 * 60 * 24));
        $seconds = $seconds % (60 * 60 * 24);
        $hour = (int)($seconds / (60 * 60));
        $seconds = $seconds % (60 * 60);
        $minute = (int)($seconds / (60));
        $seconds = $seconds % (60);
        return [ "month" => $month,
                 "week" => $week,
                 "day" => $day,
                 "hour" => $hour,
                 "minute" => $minute,
                 "second" => $seconds];
    }

    public function getString()
    {
        $res = '';
        $timeInterval = $this->getArray();
        if ($mydatetime['month'] > 0) {$res .= $mydatetime['month'] . " mois ";}
        if ($mydatetime['week'] > 0) {$res .= $mydatetime['week'] . " semaine(s) ";}
        if ($mydatetime['day'] > 0) {$res .= $mydatetime['day'] . " j ";}
        if ($mydatetime['hour'] > 0) {$res .= $mydatetime['hour'] . " h ";}
        if ($mydatetime['minute'] > 0) {$res .= $mydatetime['minute'] . " m ";}
        if ($mydatetime['second'] > 0) {$res .= $mydatetime['second'] . " s ";}
        return $res;
    }
}
