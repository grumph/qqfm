<?php

namespace capcat\QQFMBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use capcat\QQFMBundle\TimeQuantity;
use capcat\QQFMBundle\Entity\Tasks;

class TasksController extends Controller
{

    public function indexAction()
    {
        $tasksRepository = $this->getDoctrine()->getManager()->getRepository('QQFMBundle:Tasks');
        $firstTask = $tasksRepository->createQueryBuilder('t')
                   ->orderBy('t.next', 'ASC')
                   ->setMaxResults(1)
                   ->getQuery()->getOneOrNullResult();

        return $this->render('@QQFM/Tasks/index.html.twig', array('task' => $firstTask));
    }

    public function manageAction()
    {
        $tasks = $this->getDoctrine()->getManager()->getRepository('QQFMBundle:Tasks')
               ->findBy(array(), array('next' => 'ASC'));

        return $this->render('@QQFM/Tasks/tasks.html.twig', array('all_tasks' => $tasks));
    }

    public function taskAction($id, Request $request)
    {
        $entityManager = $this->getDoctrine()->getManager();

        $task = null;
        $frequency = null;
        if ($id)
        {
            $task = $entityManager->find('QQFMBundle:Tasks', $id);
            if (!$task)
            {
                $request->getSession()->getFlashBag()->add('error', 'La tâche ' . $id . " n'existe pas");
                return $this->redirectToRoute('qqfm_list_tasks');
            }
            else
            {
                $tq = new TimeQuantity($task->getFrequency());
                $frequency = $tq->getArray();
            }
        }
        return $this->render('@QQFM/Tasks/task.html.twig', array('task' => $task, 'frequency' => $frequency));
    }

    public function deleteAction($id, Request $request)
    {
        $session = $request->getSession();
        $entityManager = $this->getDoctrine()->getManager();

        $task = $entityManager->find('QQFMBundle:Tasks', $id);
        if (!$task)
        {
            $session->getFlashBag()->add('error', 'La tâche ' . $id . " n'existe pas");
        }
        else
        {
            $entityManager->remove($task);
            $entityManager->flush();
            $session->getFlashBag()->add('info', 'La tâche ' . $id . ' (' . $task->getDescription() . ') a été supprimée');
        }
        return $this->redirectToRoute('qqfm_list_tasks');
    }

    public function postponeAction($id, $seconds, Request $request)
    {
        $session = $request->getSession();
        $entityManager = $this->getDoctrine()->getManager();

        $task = $entityManager->find('QQFMBundle:Tasks', $id);
        if (!$task)
        {
            $session->getFlashBag()->add('error', 'La tâche ' . $id . " n'existe pas");
        }
        else
        {
            $next = new \DateTime();
            $next->modify('+' . $seconds . ' seconds');
            $task->setNext($next);
            $entityManager->persist($task);
            $entityManager->flush();
        }

        if ($request->headers->has('referer'))
        {
            return $this->redirect($request->headers->get('referer'));
        }
        else
        {
            return $this->redirectToRoute('qqfm_list_tasks');
        }
    }

    public function updateAction($id, Request $request)
    {
        $session = $request->getSession();
        $entityManager = $this->getDoctrine()->getManager();

        $task = null;
        if (!$id)
        {
            $task = new Tasks;
        }
        else
        {
            $task = $entityManager->find('QQFMBundle:Tasks', $id);
            if (!$task)
            {
                $session->getFlashBag()->add('error', 'La tâche ' . $id . " n'existe pas");
                return $this->redirectToRoute('qqfm_list_tasks');
            }
        }

        $task->setDescription($request->request->get('description'));

        $frequency = new TimeQuantity();
        $frequency->setFromDetails(
            $request->request->get('frequency_months', '0'),
            $request->request->get('frequency_weeks', '0'),
            $request->request->get('frequency_days', '0'),
            $request->request->get('frequency_hours', '0'),
            $request->request->get('frequency_minutes', '0'),
            $request->request->get('frequency_seconds', '0')
        );
        $task->setFrequency($frequency->get());

        $nextField = $request->request->get('next');
        if ($nextField === null)
        {
            $next = new \DateTime('now');
            $next->modify('+' . $frequency->get() . ' seconds');
        }
        else
        {
            $next = new \DateTime($nextField);
        }

        $task->setNext($next);

        $entityManager->persist($task);
        $entityManager->flush();

        if ($id)
        {
            $updateText = 'mise à jour';
        }
        else
        {
            $updateText = 'ajoutée';
        }

        $session->getFlashBag()->add('info', 'La tâche ' . $task->getId() . ' (' . $task->getDescription() . ') a été ' . $updateText);
        return $this->redirectToRoute('qqfm_list_tasks');
    }
}
