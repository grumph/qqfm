<?php

namespace capcat\QQFMBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Tasks
 *
 * @ORM\Table(name="tasks")
 * @ORM\Entity
 */
class Tasks
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="text", nullable=false)
     */
    private $description;

    /**
     * @var integer
     *
     * @ORM\Column(name="frequency", type="integer", nullable=false)
     */
    private $frequency;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="next", type="datetime", nullable=true)
     */
    private $next;

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }
    /**
     * @param string $description
     */
    public function setDescription($description)
    {
        $this->description = $description;
    }
    /**
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }
    /**
     * @param int $seconds
     */
    public function setFrequency($seconds)
    {
        $this->frequency = $seconds;
    }
    /**
     * @return int
     */
    public function getFrequency()
    {
        return $this->frequency;
    }
    /**
     * @param \DateTime $date
     */
    public function setNext($date)
    {
        $this->next = $date;
    }
    /**
     * @return \DateTime
     */
    public function getNext()
    {
        return $this->next;
    }
}

