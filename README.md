Qu'est ce Qu'on Fait Maintenant ?
=================================

Application PHP(Symfony)/HTML de gestion des tâches récurrentes.
Pour la version PHP pur, voir [ici](https://bitbucket.org/grumph/questcequonfaitmaintenant).

Captures d'écran
----------------
![Capture d'écran accueil](screenshot-main.jpg)
![Capture d'écran liste des tâches](screenshot-tasks.jpg)


Descriptif
----------
On oublie toujours des tâches récurrentes. On a tous déjà tenu un propos ressemblant à "ça fait combien de temps que j'ai changé l'eau des poissons ?", "cette plante s'arrose toutes les semaines ou tous les mois ?", "j'ai oublé mon actualisation pôle emploi !".

C'est là que QuEstCeQuOnFaitMaintenant intervient, remplissez l'application avec des tâches récurrentes, et dès qu'un moment libre arrive, allez sur la page d'accueil pour vous voir proposer un truc à faire, sans avoir à réfléchir.

Bravo, vous pouvez maintenant enlever les tâches récurrentes de votre TODO list pour vous concentrer sur les choses qui comptent.


Installation
------------
$ cd <www directory on your server>
$ git clone https://bitbucket.org/grumph/qqfm
$ cd qqfm
$ mkdir -p var/cache
$ composer install
$ php bin/console doctrine:database:create
$ php bin/console doctrine:schema:update --force


Informations techniques
-----------------------
* Symfony 3.4 (PHP 5.6)
* pdo_sqlite doit être activé dans php.ini


TODO
----
* ajouter des logs (dernière fois qu'une tâche a été faite / liste de toutes les tâches faites)
* traduction (au moins en anglais)
* Bundle pour gérer les DateInterval proprement et remplacer la classe TimeQuantity (prendre exemple sur KnpTimeBundle)
* utiliser le FormBuilder de Symfony, pour avoir un code plus propre et éviter les CSRF
* ajouter de l'ajax


Bonus
-----
[Qu'est ce qu'on fait maintenant ?](https://www.youtube.com/watch?v=BPSiB2vyHLU)
